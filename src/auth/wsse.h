//
// Created by emily on 10/22/22.
//

#pragma once

#include "soap/soap_field_base.h"
#include <memory>
#include <optional>
#include <string>

const static auto NONCE_LENGTH = 20;
const static auto SHA1_LENGTH = 20;

namespace openptz {
namespace auth {
namespace wsse {
std::array<unsigned char, NONCE_LENGTH> createRandomNonce();

std::array<unsigned char, SHA1_LENGTH>
createPasswordDigest(const std::array<unsigned char, 20> &nonce,
                     const std::string &created, const std::string &password);

std::unique_ptr<SoapFieldBase> getWSSEAuthElement(const std::string &username,
                                                  const std::string &password);

} // namespace wsse
} // namespace auth
} // namespace openptz