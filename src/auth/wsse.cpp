//
// Created by emily on 10/22/22.
//

#include <spdlog/spdlog.h>

#include <memory>

#include "logging/logging.h"
#include "soap/soap_field_recursive.h"
#include "soap/soap_field_value.h"
#include "utils/system_utils.h"

#include "utils/hash_utils.h"
#include "utils/time_utils.h"
#include "wsse.h"
#include <openssl/ssl.h>

namespace openptz {
namespace auth {
namespace wsse {

std::array<unsigned char, NONCE_LENGTH> createRandomNonce() {
  std::array<unsigned char, NONCE_LENGTH> nonce{};
  for (auto &i : nonce) {
    i = static_cast<unsigned char>(
        utils::randomIntRange(std::numeric_limits<unsigned char>::min(),
                              std::numeric_limits<unsigned char>::max()));
  }

  return nonce;
}

std::array<unsigned char, SHA1_LENGTH>
createPasswordDigest(const std::array<unsigned char, NONCE_LENGTH> &nonce,
                     const std::string &created, const std::string &password) {
  // Create sha1 context
  auto ssl_ctx = EVP_MD_CTX_new();
  EVP_DigestInit(ssl_ctx, EVP_sha1());

  EVP_DigestUpdate(ssl_ctx, &nonce[0], NONCE_LENGTH);
  EVP_DigestUpdate(ssl_ctx, created.data(), created.size());
  EVP_DigestUpdate(ssl_ctx, password.data(), password.size());

  std::array<unsigned char, SHA1_LENGTH> output{};
  EVP_DigestFinal(ssl_ctx, &output[0], nullptr);

  return output;
}

std::unique_ptr<SoapFieldBase> getWSSEAuthElement(const std::string &username,
                                                  const std::string &password) {

  auto created = utils::getUTCFormattedTimeNow();

  auto nonce = auth::wsse::createRandomNonce();
  auto nonce64 = utils::encodeBase64(&nonce[0], nonce.size());

  auto digest = auth::wsse::createPasswordDigest(nonce, created, password);
  auto digest64 = utils::encodeBase64(&digest[0], digest.size());

  auto usernameObject = std::unique_ptr<SoapFieldValue>(
      new SoapFieldValue("Username", "wsse", username));

  auto passwordObject = std::unique_ptr<SoapFieldValue>(
      new SoapFieldValue("Password", "wsse", digest64));
  passwordObject->addAttribute(
      "Type", "http://docs.oasis-open.org/wss/2004/01/"
              "oasis-200401-wss-username-token-profile-1.0#PasswordDigest");

  auto nonceObject = std::unique_ptr<SoapFieldValue>(
      new SoapFieldValue("Nonce", "wsse", nonce64));

  auto createdObject = std::unique_ptr<SoapFieldValue>(
      new SoapFieldValue("Created", "wsu", created));

  auto usernameTokenObject = std::unique_ptr<SoapFieldRecursive>(
      new SoapFieldRecursive("UsernameToken", "wsse"));

  usernameTokenObject->addField(std::move(usernameObject));
  usernameTokenObject->addField(std::move(passwordObject));
  usernameTokenObject->addField(std::move(nonceObject));
  usernameTokenObject->addField(std::move(createdObject));

  auto securityObject = std::unique_ptr<SoapFieldRecursive>(
      new SoapFieldRecursive("Security", "wsse"));
  if (!securityObject->declareNamespace("wsse") ||
      !securityObject->declareNamespace("wsu")) {
    logging::getLogger("WSSEGenerator")
        ->error(
            R"(Could not declare namespaces "wsse" and "wsu", which are mandatory)");
    return nullptr;
  }
  securityObject->addAttribute("env", "mustUnderstand", "true");

  securityObject->addField(std::move(usernameTokenObject));
  return securityObject;
}
} // namespace wsse
} // namespace auth
} // namespace openptz