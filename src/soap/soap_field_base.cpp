//
// Created by emily on 10/18/22.
//

#include "soap_field_base.h"
#include "logging/logging.h"

#include <spdlog/logger.h>

#include <sstream>
#include <utility>

using namespace openptz;

SoapFieldBase::SoapFieldBase(std::string name, std::string prefix)
    : m_name(std::move(name)), m_namespace(std::move(prefix)),
      m_logger(logging::getLogger(fmt::format("SoapField '{}'", m_name))) {}

void SoapFieldBase::addAttribute(const std::string &key, std::string value,
                                 bool overwrite) {
  if (m_attributes.find(key) != m_attributes.end()) {
    if (!overwrite) {
      m_logger->warn("Attribute already exists, ignoring");
      return;
    }
    m_logger->warn("Key {} already found in attributes, overwriting", key);
  }
  m_attributes[key] = std::move(value);
}

void SoapFieldBase::addAttribute(const std::string &key,
                                 const std::string &prefix,
                                 const std::string &value) {
  addAttribute(fmt::format("{}:{}", prefix, key), value);
}

void SoapFieldBase::setIndent(unsigned int indent) { m_indentation = indent; }

std::string SoapFieldBase::serializeIndent(unsigned int indent) {
  std::stringstream ss;
  for (unsigned int i = 0; i < indent; i++) {
    ss << "\t";
  }
  return ss.str();
}

std::string SoapFieldBase::serializeAttributes() const {
  std::stringstream ss;
  for (const auto &attr : m_attributes) {
    ss << fmt::format(" {}=\"{}\"", attr.first, attr.second);
  }
  return ss.str();
}

std::string SoapFieldBase::serializeMarkupTag(bool withClose) {
  return withClose ? fmt::format("</{}:{}>", m_namespace, m_name)
                   : fmt::format("{}<{}:{}{}>", serializeIndent(m_indentation),
                                 m_namespace, m_name, serializeAttributes());
}

SoapFieldBase::~SoapFieldBase() { logging::dropLogger(m_logger); }