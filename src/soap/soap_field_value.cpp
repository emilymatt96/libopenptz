//
// Created by emily on 10/18/22.
//
#include "soap_field_value.h"
#include <spdlog/fmt/fmt.h>
#include <sstream>
#include <utility>

using namespace openptz;

SoapFieldValue::SoapFieldValue(std::string name, std::string prefix,
                               std::string value)
    : SoapFieldBase(std::move(name), std::move(prefix)),
      m_value(std::move(value)) {}

SoapFieldValue::~SoapFieldValue() = default;

std::string SoapFieldValue::serialize() {
  return fmt::format("{}{}{}", serializeMarkupTag(), m_value,
                     serializeMarkupTag(true));
}
