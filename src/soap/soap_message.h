//
// Created by emily on 10/18/22.
//

#pragma once

#include "soap_field_recursive.h"
#include <unordered_set>

namespace openptz {
class SoapMessage : public SoapFieldRecursive {
protected:
  std::list<std::unique_ptr<SoapFieldBase>> m_headers;
  std::unordered_set<std::string> m_namespaces;

public:
  SoapMessage();

  ~SoapMessage() override;

  void addHeader(std::unique_ptr<SoapFieldBase> header);

  void finalize();

  std::string serialize() override;
};
} // namespace openptz
