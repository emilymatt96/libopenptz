//
// Created by emily on 10/18/22.
//

#include "soap_field_recursive.h"
#include "openptz.h"

#include <numeric>
#include <spdlog/spdlog.h>
#include <sstream>
#include <utility>

using namespace openptz;

SoapFieldRecursive::SoapFieldRecursive(std::string name, std::string prefix)
    : SoapFieldBase(std::move(name), std::move(prefix)) {}

SoapFieldRecursive::~SoapFieldRecursive() = default;

void SoapFieldRecursive::addField(std::unique_ptr<SoapFieldBase> field) {
  field->setIndent(m_indentation + 1);
  m_sub_fields.push_back(std::move(field));
}

void SoapFieldRecursive::setIndent(unsigned int indent) {
  m_indentation = indent;

  for (const auto &ptr : m_sub_fields) {
    ptr->setIndent(m_indentation + 1);
  }
}

std::string SoapFieldRecursive::serialize() {
  std::string ss;
  auto fields = std::accumulate(
      m_sub_fields.begin(), m_sub_fields.end(), std::string(),
      [](std::string &first, const std::unique_ptr<SoapFieldBase> &field)
          -> std::string { return first += field->serialize() + '\n'; });

  /***
   * The expected output format is as follows:
   * (Indentation)<MarkupTag with attributes>\n
   * (All the subfields, with a newline between each one)
   * (If there were no fields, add a newline manually to keep things pretty)
   * (Indentation)</MarkupTag>
   ***/
  return fmt::format("{}\n"
                     "{}{}"
                     "{}{}",
                     serializeMarkupTag(), fields, fields.empty() ? "\n" : "",
                     serializeIndent(m_indentation), serializeMarkupTag(true));
}

bool SoapFieldRecursive::declareNamespace(const std::string &prefix) {
  std::string definition;
  if (!getDefinitionForNamespace(prefix, definition)) {
    return false;
  }
  addAttribute(prefix, "xmlns", definition);
  return true;
}