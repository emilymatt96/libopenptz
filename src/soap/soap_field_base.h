//
// Created by emily on 10/18/22.
//

#pragma once

#include <list>
#include <memory>
#include <string>
#include <unordered_map>

namespace spdlog {
class logger;
}

namespace openptz {
class SoapFieldBase {
protected:
  std::string m_name;
  std::string m_namespace;
  std::unordered_map<std::string, std::string> m_attributes;
  std::shared_ptr<spdlog::logger> m_logger;
  unsigned int m_indentation{0};

  std::string serializeAttributes() const;

  std::string serializeMarkupTag(bool withClose = false);

public:
  explicit SoapFieldBase(std::string name, std::string prefix);

  virtual ~SoapFieldBase();

  virtual std::string serialize() = 0;

  virtual void setIndent(unsigned int indent);

  void addAttribute(const std::string &key, std::string value,
                    bool overwrite = false);

  void addAttribute(const std::string &key, const std::string &prefix,
                    const std::string &value);

  static std::string serializeIndent(unsigned int indent);
};
} // namespace openptz
