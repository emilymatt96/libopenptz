//
// Created by emily on 10/18/22.
//

#include <memory>
#include <spdlog/logger.h>
#include <sstream>
#include <utility>

#include "openptz.h"
#include "soap_message.h"

using namespace openptz;

SoapMessage::SoapMessage() : SoapFieldRecursive("Envelope", "env") {
  if (!declareNamespace("env")) {
    throw std::runtime_error(
        "Could not find namespace \"env\", which is mandatory");
  }
}

SoapMessage::~SoapMessage() = default;

void SoapMessage::finalize() {
  // Forward headers into the "Header" object
  auto header = std::unique_ptr<SoapFieldRecursive>(
      new SoapFieldRecursive("Header", "env"));
  for (auto &field : m_headers) {
    header->addField(std::move(field));
  }
  m_headers.clear();

  // Forward fields into the "Body" object
  auto body = std::unique_ptr<SoapFieldRecursive>(
      new SoapFieldRecursive("Body", "env"));
  for (auto &field : m_sub_fields) {
    body->addField(std::move(field));
  }
  m_sub_fields.clear();

  // Doing this ensures this object only has the "Header" and "Body objects
  addField(std::move(header));
  addField(std::move(body));
}

std::string SoapMessage::serialize() {
  finalize();

  return fmt::format("<?xml version=\"1.0\"?>\n"
                     "{}",
                     SoapFieldRecursive::serialize());
}

void SoapMessage::addHeader(std::unique_ptr<SoapFieldBase> header) {
  header->setIndent(m_indentation + 1);
  m_headers.push_back(std::move(header));
}
