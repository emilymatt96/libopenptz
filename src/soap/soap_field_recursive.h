//
// Created by emily on 10/18/22.
//

#pragma once

#include "soap_field_base.h"

namespace openptz {
class SoapFieldRecursive : public SoapFieldBase {
protected:
  std::list<std::unique_ptr<SoapFieldBase>> m_sub_fields;

public:
  SoapFieldRecursive(std::string name, std::string prefix);

  ~SoapFieldRecursive() override;

  void addField(std::unique_ptr<SoapFieldBase> field);

  void setIndent(unsigned int indent) override;

  bool declareNamespace(const std::string &prefix);

  std::string serialize() override;
};
} // namespace openptz