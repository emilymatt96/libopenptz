//
// Created by emily on 10/18/22.
//

#pragma once

#include "soap_field_base.h"

namespace openptz {
class SoapFieldValue : public SoapFieldBase {
protected:
  std::string m_value;

public:
  SoapFieldValue(std::string name, std::string prefix, std::string value);

  ~SoapFieldValue() override;

  std::string serialize() override;
};
} // namespace openptz