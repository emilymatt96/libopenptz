#include "openptz.h"

#include <curlpp/cURLpp.hpp>
#include <openssl/evp.h>
#include <openssl/ssl.h>
#include <spdlog/spdlog.h>

#include "logging/logging.h"

std::unordered_map<std::string, std::string> kNamespacesMap = {
    // W3
    {"env", "http://www.w3.org/2003/05/soap-envelope"},
    {"soapenc", "http://www.w3.org/2003/05/soap-encoding"},
    {"wsa", "http://www.w3.org/2005/08/addressing"},
    {"wsaw", "http://www.w3.org/2006/05/addressing/wsdl"},
    {"xs", "http://www.w3.org/2001/XMLSchema"},
    {"xsi", "http://www.w3.org/2001/XMLSchema-instance"},
    // ONVIF v10
    {"ter", "http://www.onvif.org/ver10/error"},
    {"tt", "http://www.onvif.org/ver10/schema"},
    {"tns1", "http://www.onvif.org/ver10/topics"},
    {"tmd", "http://www.onvif.org/ver10/deviceIO/wsdl"},
    {"tds", "http://www.onvif.org/ver10/device/wsdl"},
    {"trt", "http://www.onvif.org/ver10/media/wsdl"},
    {"trc", "http://www.onvif.org/ver10/recording/wsdl"},
    {"tse", "http://www.onvif.org/ver10/search/wsdl"},
    {"trp", "http://www.onvif.org/ver10/replay/wsdl"},
    {"tas", "http://www.onvif.org/ver10/advancedsecurity/wsdl"},
    {"tev", "http://www.onvif.org/ver10/events/wsdl"},
    {"tst", "http://www.onvif.org/ver10/storage/wsdl"},
    {"dn", "http://www.onvif.org/ver10/network/wsdl"},
    // ONVIF v20
    {"timg", "http://www.onvif.org/ver20/imaging/wsdl"},
    {"tan", "http://www.onvif.org/ver20/analytics/wsdl"},
    {"tr2", "http://www.onvif.org/ver20/media/wsdl"},
    {"axt", "http://www.onvif.org/ver20/analytics"},
    {"tptz", "http://www.onvif.org/ver20/ptz/wsdl"},
    // XML-SOAP
    {"wsdl", "http://schemas.xmlsoap.org/wsdl"},
    {"d", "http://schemas.xmlsoap.org/ws/2005/04/discovery"},
    {"wsadis", "http://schemas.xmlsoap.org/ws/2004/08/addressing"},
    {"http", "http://schemas.xmlsoap.org/wsdl/http"},
    {"wsoap12", "http://schemas.xmlsoap.org/wsdl/soap12"},
    // OASIS
    {"wsnt", "http://docs.oasis-open.org/wsn/b-2"},
    {"wstop", "http://docs.oasis-open.org/wsn/t-1"},
    {"wsntw", "http://docs.oasis-open.org/wsn/bw-2"},
    {"wsrf-rw", "http://docs.oasis-open.org/wsrf/rw-2"},
    {"wsrf-bf", "http://docs.oasis-open.org/wsrf/bf-2"},
    {"wsrf-r", "http://docs.oasis-open.org/wsrf/r-2"},
    {"wsse", "http://docs.oasis-open.org/wss/2004/01/"
             "oasis-200401-wss-wssecurity-secext-1.0.xsd"},
    {"wsu", "http://docs.oasis-open.org/wss/2004/01/"
            "oasis-200401-wss-wssecurity-utility-1.0.xsd"}};

void openptz::init() {
  curlpp::initialize();
  logging::setupDebugLevel("OPENPTZ_DEBUG");

  OPENSSL_init_ssl(0, nullptr);
  OpenSSL_add_all_algorithms();
  OpenSSL_add_all_digests();

  logging::getLogger("OpenPTZ")->info("OpenSSL context initiated");
}

bool openptz::registerNamespace(const std::string &abbrev,
                                const std::string &definition, bool overwrite) {
  if (abbrev.empty() || definition.empty()) {
    logging::getLogger("OpenPTZ")->error(
        R"(Cannot register XML namespace "{}" with definition "{}", is one of them empty?)",
        abbrev, definition);
    return false;
  }

  auto element = kNamespacesMap.find(abbrev);
  if (element != kNamespacesMap.end()) {
    if (!overwrite) {
      logging::getLogger("OpenPTZ")->warn(
          "Namespace \"{}\", already exist, ignoring");
      return false;
    }
    logging::getLogger("OpenPTZ")->warn("Overwriting namespace \"{}\"", abbrev);
  }
  kNamespacesMap[abbrev] = definition;
  logging::getLogger("OpenPTZ")->info("Registered namespace");
  logging::getLogger("OpenPTZ")->info("{}: {}", abbrev, definition);
  return true;
}

bool openptz::getDefinitionForNamespace(const std::string &ns,
                                        std::string &out) {
  if (kNamespacesMap.find(ns) != kNamespacesMap.end()) {
    out = kNamespacesMap[ns];
    return true;
  }
  return false;
}

void openptz::deinit() {
  curlpp::terminate();
  logging::getLogger("OpenPTZ")->info("curlPP context terminated");
  logging::dropAllLoggers();
}
