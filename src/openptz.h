#pragma once
#include <optional>
#include <string>

namespace openptz {

void init();

bool registerNamespace(const std::string &abbrev, const std::string &definition,
                       bool overwrite = false);

bool getDefinitionForNamespace(const std::string &ns, std::string &out);

void deinit();
} // namespace openptz