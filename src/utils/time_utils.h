//
// Created by emily on 10/22/22.
//

#pragma once

#include <string>

namespace openptz {
namespace utils {
std::string getUTCFormattedTimeNow();
}
} // namespace openptz
