//
// Created by emily on 10/22/22.
//

#include "time_utils.h"
#include <iomanip>
#include <sstream>

namespace openptz {
namespace utils {
std::string getUTCFormattedTimeNow() {
  time_t rawTime;
  time(&rawTime);

  std::stringstream ss;
  ss << std::put_time(localtime(&rawTime), "%Y-%m-%dT%H:%M:%SZ");
  return ss.str();
}
} // namespace utils
} // namespace openptz