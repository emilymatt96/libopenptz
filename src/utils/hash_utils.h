//
// Created by emily on 10/22/22.
//

#pragma once

#include <string>

namespace openptz {
namespace utils {
std::string encodeBase64(unsigned char *input, unsigned int size,
                         unsigned int reqSize = 0);
} // namespace utils
} // namespace openptz