//
// Created by emily on 10/28/22.
//

#pragma once

#include <random>
namespace openptz {
namespace utils {
template <typename T> T randomIntRange(T min, T max) {
  std ::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<T> dist(min, max);
  return dist(mt);
}

template <typename T> T randomFloatRange(T min, T max) {
  std ::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_real_distribution<T> dist(min, max);
  return dist(mt);
}
} // namespace utils
} // namespace openptz
