//
// Created by emily on 10/22/22.
//

#include "hash_utils.h"
#include <openssl/evp.h>

namespace openptz {
namespace utils {
std::string encodeBase64(unsigned char *input, unsigned int size,
                         unsigned int reqSize) {
  // EVP adds an annoying null char at the end
  auto newSize = reqSize ? reqSize + 1 : (4 * ((size + 2) / 3)) + 1;
  auto output = reinterpret_cast<char *>(malloc(newSize));

  EVP_EncodeBlock(reinterpret_cast<unsigned char *>(output), input,
                  static_cast<int>(size));

  return output;
}
} // namespace utils
} // namespace openptz