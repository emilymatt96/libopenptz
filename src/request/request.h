//
// Created by emily on 10/18/22.
//

#pragma once

#include <list>
#include <memory>
#include <optional>
#include <string>

namespace spdlog {
class logger;
}

namespace curlpp {
class Easy;
}

namespace openptz {
class SoapMessage;

class Request {
private:
  std::shared_ptr<spdlog::logger> m_logger;
  std::unique_ptr<curlpp::Easy> m_handle;
  std::string m_url;
  std::list<std::string> m_headers;

public:
  explicit Request(std::string url);

  void addHeader(const std::string &header_name,
                 const std::string &header_value);

  bool send(const std::string &msg, std::string &out);

  bool send(const std::shared_ptr<SoapMessage> &msg, std::string &out);

  ~Request();
};
} // namespace openptz
