//
// Created by emily on 10/18/22.
//

#include "request.h"
#include "logging/logging.h"
#include "soap/soap_message.h"

#include <curlpp/Easy.hpp>
#include <curlpp/Infos.hpp>
#include <curlpp/Options.hpp>
#include <spdlog/logger.h>

#include <sstream>
#include <utility>

using namespace openptz;

Request::Request(std::string url)
    : m_logger(logging::getLogger("Request")), m_handle(new curlpp::Easy()),
      m_url(std::move(url)) {
  m_handle->setOpt(curlpp::options::Url(m_url));
}

Request::~Request() { logging::dropLogger(m_logger); }

void Request::addHeader(const std::string &header_name,
                        const std::string &header_value) {
  m_headers.push_back(fmt::format("{}: {}", header_name, header_value));
}

bool Request::send(const std::string &msg, std::string &out) {
  std::stringstream responseStream;

  m_logger->trace("Sending message:\n{}", msg);
  try {
    m_handle->setOpt(curlpp::options::FailOnError(false));
    m_handle->setOpt(curlpp::options::HttpHeader(m_headers));
    m_handle->setOpt(curlpp::options::PostFields(msg));
    m_handle->setOpt(curlpp::options::SslVerifyPeer(false));
    m_handle->setOpt(
        curlpp::options::PostFieldSize(static_cast<long>(msg.size())));
    m_handle->setOpt(curlpp::options::WriteStream(&responseStream));
    m_handle->perform();
  } catch (std::exception &e) {
    m_logger->error(e.what());
  }

  out = responseStream.str();
  return curlpp::infos::ResponseCode::get(*m_handle) > 200 &&
         curlpp::infos::ResponseCode::get(*m_handle) < 210;
}

bool Request::send(const std::shared_ptr<SoapMessage> &msg, std::string &out) {
  return send(msg->serialize(), out);
}