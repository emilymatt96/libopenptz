//
// Created by emily on 10/18/22.
//

#pragma once

#include <memory>
#include <string>

namespace spdlog {
class logger;
}

namespace openptz {
namespace logging {
void setupDebugLevel(const std::string &envName);

std::shared_ptr<spdlog::logger> getLogger(const std::string &logger_name);

void dropLogger(const std::shared_ptr<spdlog::logger> &logger);

void dropAllLoggers();
} // namespace logging
} // namespace openptz
