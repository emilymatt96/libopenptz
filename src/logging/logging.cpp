//
// Created by emily on 10/18/22.
//

#include "logging.h"
#include <spdlog/spdlog.h>

namespace openptz {
namespace logging {
// To ensure the entire program's getting and deleting of loggers is ok
static std::mutex kLoggersMutex;

void setupDebugLevel(const std::string &envName) {
  int logLevelParsed = 2;
  auto lvl = std::getenv(envName.c_str());
  if (lvl) {
    try {
      logLevelParsed = std::stoi(lvl);
    } catch (...) {
      spdlog::error("Could not parse env variable {}, using default log level",
                    envName);
    }
  } else {
    spdlog::info("Environment variable not set, Using default debug level");
  }

  auto logLevelInEnum = static_cast<spdlog::level::level_enum>(
      std::max(std::min(logLevelParsed, 6), 0));
  spdlog::info("Debug level: {}",
               spdlog::level::to_string_view(logLevelInEnum));
  spdlog::set_level(logLevelInEnum);
}

std::shared_ptr<spdlog::logger> getLogger(const std::string &logger_name) {
  std::lock_guard<std::mutex> loggersLock(kLoggersMutex);
  auto logger = spdlog::get(logger_name);
  if (!logger) {
    logger =
        spdlog::create<spdlog::sinks::ansicolor_stdout_sink_mt>(logger_name);
  }
  return logger;
}

void dropLogger(const std::shared_ptr<spdlog::logger> &logger) {
  std::lock_guard<std::mutex> loggersLock(kLoggersMutex);
  spdlog::drop(logger->name());
}

void dropAllLoggers() {
  std::lock_guard<std::mutex> loggersLock(kLoggersMutex);
  spdlog::drop_all();
}
} // namespace logging
} // namespace openptz
