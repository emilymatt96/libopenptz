//
// Created by emily on 10/18/22.
//
#include "auth/wsse.h"
#include "openptz.h"
#include "request/request.h"
#include "soap/soap_field_value.h"
#include "soap/soap_message.h"
#include "utils/hash_utils.h"
#include "utils/time_utils.h"

#include <spdlog/spdlog.h>

using namespace openptz;

/***
 * This example creates a WSSE signed SOAP message and sends it to the requested
 *server
 * TODO: create the other half that receives this message and ensures it is
 *valid
 ***/
int main(int /*argc*/, char ** /*argv*/) {
  openptz::init();

  auto message = std::make_shared<SoapMessage>();
  message->declareNamespace("tt");
  message->declareNamespace("tptz");

  auto wsse = auth::wsse::getWSSEAuthElement("admin", "Emily~Matt1996");
  if (!wsse) {
    spdlog::critical("Could not create WSSE SOAP element");
    return -1;
  }

  auto getConfigurations = std::unique_ptr<SoapFieldValue>(
      new SoapFieldValue("GetConfigurations", "tptz", ""));

  message->addHeader(std::move(wsse));
  message->addField(std::move(getConfigurations));

  Request request("http://10.0.0.30/onvif/device_service");
  request.addHeader("Content-Type", "application/xml+soap; charset=utf-8");
  std::string response;
  auto status = request.send(message, response);
  if (!response.empty()) {
    spdlog::info(response);
  }

  openptz::deinit();
  return status ? 0 : 1;
}