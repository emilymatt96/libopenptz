add_executable(example_wsse wsse/example_wsse.cpp)

target_link_libraries(example_wsse openptz ${CURLPP_LIBRARIES} ${UUID_LIBRARIES})
