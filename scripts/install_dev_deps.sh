#!/bin/bash

set -e

echo "Refreshing package list"
apt update

echo "Installing dependencies"
apt install -y --no-install-recommends \
  build-essential \
  cmake \
  gcc \
  g++ \
  libcurlpp-dev \
  libcurl4-openssl-dev \
  libspdlog-dev \
  libssl-dev \
  make \
  pkg-config \
  uuid-dev

echo "Performing system update"
apt autoremove -y
apt upgrade -y

echo "Cleaning up"
apt clean
