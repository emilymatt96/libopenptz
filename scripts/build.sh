#!/bin/bash

set -e

echo "Running CMake"
cmake .

echo "Compiling"
make "-j$(nproc --ignore=2)"

echo "Installing Files"
make install DESTDIR=/output
